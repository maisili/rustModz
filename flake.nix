{
  description = "rustModz";

  inputs = {
    nixrs = {
      url = file:///home/li/dev/nixrs;
      ref = "main";
      type = "git";
    };
  };

  outputs = fleiks @{ self, ... }:
  {
    datom = { sobUyrld =
      {
        lamdy = uyrld @{ kor, lib, bildRust, }:
        let
          modFleiks = builtins.removeAttrs fleiks [ "self" ];

          mkRustMod = neim: fleik @{ datom, ... }:
          let
            spici = kor.mkNioSpici {
              inherit datom;
              spek = {
                rustMod = {
                  no_std = false;
                  nightly = false;
                  modzSet = {};
                  indeksCrates = {};
                  features = [ "default" ];
                  iuniksSors = fleik + /src/lib.rs;
                  vyrzyn = fleik.shortRev;
                  ovyraidz = {};
                };
              };
            };

            modNeimz = builtins.attrNames modFleiks;

            matcMod = modNeim: ovyraidz:
            assert kor.mesydj (builtins.hasAttr modNeim modNeimz)
            "RustMod ${modNeim} doesn't exist";
            let
              hazOvyraidz = ovyraidz != {};
              mod = rustModz.${modNeim};
            in
            if hazOvyraidz then
            mod.override ovyraidz
            else mod;

            matcModz = builtins.mapAttrs matcMod spici.datom.modzSet;

            hazModz = (spici.datom.modzSet != {});

            argz = (builtins.removeAttrs spici.datom [ "ovyraidz" ])
            // {
              inherit neim;
              modzSet = kor.optionalAttrs hazModz matcModz;
              spici = "mod";
            };

            bildLamdy = bildRustArgz: bildRust bildRustArgz;

            rustMod = lib.makeOverridable bildLamdy argz;

          in rustMod;

          rustModz = builtins.mapAttrs mkRustMod modFleiks;

        in rustModz;
      };
    };
  };
}
